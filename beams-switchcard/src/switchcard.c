/* --------------------------------------------------------------------------                                 
    Template project main
    File name: switchcard.c
    Author: Anmol Thind
    Description: The template main file. It provides a simple example of using
                 some standard scandal functions such as the UART library, the
                 CAN library, timers, LEDs, GPIOs.
                 It is designed to compile and work for the 3 micros we use on
                 the car currently, the MSP430F149 and MCP2515 combination and
                 the LPC11C14 and LPC1768 which have built in CAN controllers.

                 UART_printf is provided by the Scandal stdio library and 
                 should work well on all 3 micros.

                 If you are using this as the base for a new project, you
                 should first change the name of this file to the name of
                 your project, and then in the top level makefile, you should
                 change the CHIP and MAIN_NAME variables to correspond to
                 your hardware.

                 Don't be scared of Scandal. Dig into the code and try to
                 understand what's going on. If you think of an improvement
                 to any of the functions, then go ahead and implement it.
                 However, before committing the changes to the Scandal repo,
                 you should discuss with someone else to ensure that what 
                 you've done is a good thing ;-)

                 Keep in mind that this code is live to the public on
                 Google Code. No profanity in comments please!

    Copyright (C) Etienne Le Sueur, 2011

    Created: 07/09/2011
   -------------------------------------------------------------------------- */

#include <scandal/engine.h>
#include <scandal/message.h>
#include <scandal/leds.h>
#include <scandal/utils.h>
#include <scandal/uart.h>
#include <scandal/stdio.h>
#include <scandal/wdt.h>
#include <scandal/wavesculptor.h>

#include <string.h>

#include <project/driver_config.h>
#include <project/target_config.h>
#include <arch/can.h>
#include <arch/uart.h>
#include <arch/timer.h>
#include <arch/gpio.h>
#include <arch/types.h>
#include <arch/i2c.h>

void debug_switches(void);

/* Do some general setup for clocks, LEDs and interrupts
 * and UART stuff on the MSP430 */
void setup(void) {
	GPIO_Init();
	GPIO_SetDir(RED_LED_PORT, RED_LED_BIT, 1);
	GPIO_SetDir(YELLOW_LED_PORT, YELLOW_LED_BIT, 1);

	GPIO_SetDir(CH1_PORT, CH1_PIN, 1);
	GPIO_SetDir(CH2_PORT, CH2_PIN, 1);
	GPIO_SetDir(CH3_PORT, CH3_PIN, 1);
	GPIO_SetDir(CH4_PORT, CH4_PIN, 1);

	GPIO_SetValue(CH1_PORT, CH1_PIN, 0); 
	GPIO_SetValue(CH2_PORT, CH2_PIN, 0);
	GPIO_SetValue(CH3_PORT, CH3_PIN, 0);
	GPIO_SetValue(CH4_PORT, CH4_PIN, 0);

} // setup

/* This is an in-channel handler. It gets called when a message comes in on the
 * registered node/channel combination as set up using the USB-CAN. */
void switch_handler(int32_t value, uint32_t src_time) {
	UART_printf("switch_handler got called with value %d time at source %u\n\r", (int)value, (unsigned int)src_time);

	int onoff = value & OFFSET_ONOFF;
	value &= ~OFFSET_ONOFF;

	switch(value){
		case OFFSET_HIGHBEAM:
			UART_printf("High beam state: %d \n\r", onoff);
			GPIO_SetValue(CH2_PORT, CH1_PIN, onoff);
			GPIO_SetValue(CH4_PORT, CH3_PIN, onoff);
			break;

		case OFFSET_LOWBEAM:
			UART_printf("Low beam state: %d \n\r", onoff);
			GPIO_SetValue(CH1_PORT, CH2_PIN, onoff);
			GPIO_SetValue(CH3_PORT, CH4_PIN, onoff);
			break;

		default:
			UART_printf("No high beam, low beam related toggles");
			break;
	}
}

/* This is your main function! You should have an infinite loop in here that
 * does all the important stuff your node was designed for */
int main(void) {

	setup();

	/* Initialise the watchdog timer. If the node crashes, it will restart automatically */
	WDT_Init(); 

	/* Initialise Scandal, registers for the config messages, timesync messages and in channels */
	scandal_init();

	/* Initialise UART0 */
	UART_Init(115200);

	/* Wait until UART is ready */
	scandal_delay(100);

	/* Display welcome header over UART */
	UART_printf("Welcome to the switchcard project! This card controls high beams and low beams.\n\r");

	scandal_register_in_channel_handler(0, &switch_handler);

	sc_time_t one_sec_timer = sc_get_timer();

	/* This is the main loop, go for ever! */
	while (1) {
		/* This checks whether there are pending requests from CAN, and sends a heartbeat message.
		 * The heartbeat message encodes some data in the first 4 bytes of the CAN message, such as
		 * the number of errors and the version of scandal */
		handle_scandal();
		
		/* Tickle the watchdog so we don't reset */
		WDT_Feed();
	}
}

void debug_switches(void)
{
	GPIO_SetValue(CH1_PORT, CH1_PIN, 1);
	GPIO_SetValue(CH2_PORT, CH2_PIN, 0);
	GPIO_SetValue(CH3_PORT, CH3_PIN, 1);
	GPIO_SetValue(CH4_PORT, CH4_PIN, 0);
}