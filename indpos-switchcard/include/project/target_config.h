#define RED_LED_PORT 		2		// Port for led
#define RED_LED_BIT 		8		// Bit on port for led
#define YELLOW_LED_PORT 	2		// Port for led
#define YELLOW_LED_BIT 		7		// Bit on port for led

#define CH1_PORT		 	1
#define CH1_PIN				2

#define CH2_PORT			1
#define CH2_PIN				0

#define CH3_PORT			3
#define CH3_PIN				0

#define CH4_PORT			1
#define CH4_PIN				10

#define CH5_PORT			0
#define CH5_PIN				9

#define CH6_PORT			0
#define CH6_PIN				2

#define CH7_PORT			2
#define CH7_PIN				2

#define CH8_PORT			2
#define CH8_PIN				11

// Offsets for 32 bit flags for communicating between center/dash/switchcards
/*
#define OFFSET_ONOFF				0x1		// Enable (1) or disable (0)
#define OFFSET_INDICATOR_LEFT		0x2
#define OFFSET_INDICATOR_RIGHT		0x4
#define OFFSET_PARKING_LIGHT		0x8
#define OFFSET_LOWBEAM				0x10
#define OFFSET_HIGHBEAM				0x20
#define OFFSET_PARKED				0x40
#define OFFSET_NEUTRAL				0x80
#define OFFSET_DRIVE				0x100
#define OFFSET_ECO					0x200
#define OFFSET_REVERSE				0x400
#define OFFSET_HANDBRAKE			0x800
#define OFFSET_BRAKE_FAIL			0x1000
#define OFFSET_CRUISE				0x2000
#define OFFSET_LOW_FLUID			0x4000
#define OFFSET_LIGHTS_FAIL			0x8000
#define OFFSET_HORN					0x10000
#define OFFSET_HAZARDS				0x20000
#define OFFSET_DEMISTER				0x40000
#define OFFSET_WIPER_INT			0x80000
#define OFFSET_WIPER_LOW			0x100000
#define OFFSET_WIPER_HIGH			0x200000
#define OFFSET_WIPER_WASH			0x400000
*/

// Offsets for 32 bit flags for communicating between center/dash
#define OFFSET_ONOFF                0x1     // Enable (1) or disable (0)

#define OFFSET_INDICATOR_LEFT       0x2
#define OFFSET_INDICATOR_RIGHT      0x4

#define OFFSET_PARKED               0x8
#define OFFSET_REVERSE              0x10
#define OFFSET_NEUTRAL              0x20
#define OFFSET_DRIVE                0x40
#define OFFSET_ECO                  0x80
#define OFFSET_CRUISE_TOGGLE        0x100

#define OFFSET_HANDBRAKE            0x200
#define OFFSET_HANDCHECK            0x400
#define OFFSET_BRAKE_FAIL           0x800
#define OFFSET_LIGHTS_FAIL          0x1000
#define OFFSET_CONTROL_FAILURE      0x2000
#define OFFSET_WARNING_NONSPECIFIC  0x4000

#define OFFSET_LOW_BATTERY          0x8000
#define OFFSET_POSITION_LIGHT       0x10000
#define OFFSET_LOWBEAM              0x20000
#define OFFSET_HIGHBEAM             0x40000
#define OFFSET_SEATBELT_SENSOR      0x80000

#define OFFSET_HAZARDS              0x400000

//Eco was flickering because offset braking is the same as eco

#define OFFSET_BRAKING                 0x200000 //Brake Lights for SUTI, its fine to keep this the same as cruise toggle.

#define FLASH_PERIOD 		330