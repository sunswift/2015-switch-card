#define RED_LED_PORT 		2		// Port for led
#define RED_LED_BIT 		8		// Bit on port for led
#define YELLOW_LED_PORT 	2		// Port for led
#define YELLOW_LED_BIT 		7		// Bit on port for led

#define CH1_PORT		 	1
#define CH1_PIN				5

#define CH2_PORT			1
#define CH2_PIN				11

#define CH3_PORT			1
#define CH3_PIN				4

#define CH4_PORT			2
#define CH4_PIN				3
